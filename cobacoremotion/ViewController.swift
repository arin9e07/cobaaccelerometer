//
//  ViewController.swift
//  cobacoremotion
//
//  Created by arinal haq on 23/09/20.
//  Copyright © 2020 arinal haq. All rights reserved.
//

import UIKit
import CoreMotion
import AudioToolbox

class ViewController: UIViewController {
    
    @IBOutlet weak var kocok: UILabel!
    @IBOutlet weak var x: UILabel!
    @IBOutlet weak var y: UILabel!
    @IBOutlet weak var z: UILabel!
    var speed:Double = 0.0
    let motion = CMMotionManager()
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        startAccelerometers()
        print("duh")
    }

    func startAccelerometers() {
       // Make sure the accelerometer hardware is available.
       if self.motion.isAccelerometerAvailable {
        print("dah")
          self.motion.accelerometerUpdateInterval = 1.0 / 60.0  // 60 Hz
        self.motion.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data{
                
//                print(myData)
//                self.speed += myData.acceleration.x * (1.0/60.0)
//                print(self.speed)
                self.x.text = "\(myData.acceleration.x)"
                self.y.text = "\(myData.acceleration.y)"
                self.z.text = "\(myData.acceleration.z)"
                
                if myData.acceleration.x >= 2 || myData.acceleration.x <= -2{
                    if myData.acceleration.x <= -2{
                        self.kocok.text = "kocok kiri \(myData.acceleration.x)"
                    }else{
                        self.kocok.text = "kocok kanan \(myData.acceleration.x)"
                    }
                    
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                }else if myData.acceleration.y >= 2 || myData.acceleration.y <= -2{
                    if myData.acceleration.y <= -2{
                        self.kocok.text = "kocok bawah \(myData.acceleration.y)"
                    }else{
                        self.kocok.text = "kocok atas \(myData.acceleration.y)"
                    }
                    self.y.text = "\(myData.acceleration.y)"
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                }else if myData.acceleration.z >= 2 || myData.acceleration.z <= -2{
                    if myData.acceleration.z <= -2{
                        self.kocok.text = "kocok mundur \(myData.acceleration.z)"
                    }else{
                        self.kocok.text = "kocok maju \(myData.acceleration.z)"
                    }
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                }
                
//                print(myData.acceleration.y)
//                print(myData.acceleration.z)
            }
        }

//          // Configure a timer to fetch the data.
//          self.timer = Timer(fire: Date(), interval: (1.0/60.0),
//                repeats: true, block: { (timer) in
//             // Get the accelerometer data.
//             if let data = self.motion.accelerometerData {
//                let x = data.acceleration.x
//                let y = data.acceleration.y
//                let z = data.acceleration.z
//                print(z)
//                // Use the accelerometer data in your app.
//             }
//          })
            
          // Add the timer to the current run loop.
        //RunLoop.current.add(self.timer, forMode: .RunLoop.Mode.default)
       }
    }


}

